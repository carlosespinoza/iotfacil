#include <ESP8266WiFi.h>
#include <WiFiManager.h>  
#include <PubSubClient.h>
WiFiClient espClient2mqtt;
PubSubClient mqtt(espClient2mqtt);
int pinLed=D1;
int pinRele=D0;
int pinLDR=A0;
String comando;
bool estatusLed;
bool estatusRele;
void setup() {
  pinMode(pinLed,OUTPUT);
  pinMode(pinRele,OUTPUT);
  pinMode(pinLDR,INPUT);
  comando="";
  Led(false);
  Rele(false);
  WiFiManager wifiManager;
  wifiManager.autoConnect("IoTFacil");
  mqtt.setServer("broker.hivemq.com",1883);
  mqtt.setCallback(callback);
  ConectarMQTT();
}

void loop() {
  mqtt.loop();
  if(comando!=""){
    if(comando=="L1"){
      Led(true);
    }
    if(comando=="L0"){
      Led(false);
    }
    if(comando=="R1"){
      Rele(true);
    }
    if(comando=="R0"){
      Rele(false);
    }
    if(comando=="L?"){
      EstatusLed();
    }
    if(comando=="R?"){
      EstatusRele();
    }
    if(comando=="F?"){
      LeeLDR();
    }
    comando="";
  }
}

void EstatusLed(){
  if(estatusLed){
    EnviarComando("L=1");
  }else{
    EnviarComando("L=0");
  }
}

void EstatusRele(){
  if(estatusRele){
    EnviarComando("R=1");
  }else{
    EnviarComando("R=0");
  }
}

void Led(bool encender){
  estatusLed=encender;
  if(encender){
    digitalWrite(pinLed,HIGH);
    EnviarComando("L=1");
  }else{
    digitalWrite(pinLed,LOW);
    EnviarComando("L=0");
  }
}

void Rele(bool encender){
  estatusRele=encender;
  if(encender){
    digitalWrite(pinRele,HIGH);
    EnviarComando("R=1");
  }else{
    digitalWrite(pinRele,LOW);
    EnviarComando("R=0");
  }
}

void LeeLDR(){
  EnviarComando("F="+String(analogRead(pinLDR)));
}

void ConectarMQTT(){
  if(!mqtt.connected()){
    mqtt.connect("IoTFacil");
    mqtt.subscribe("IoTFacil/Dispositivo");
    EnviarComando("Tarjeta IoTFacil01 conectada");
  }
}
void EnviarComando(String comando){
  Serial.println(comando);
  int len=comando.length()+1;
  char cmd[len];
  comando.toCharArray(cmd,len);
  mqtt.publish("IoTFacil/App",cmd);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Llego comando por MQTT [");
  Serial.print(topic);
  Serial.print("] [");
  String temp="";
  int i=0;
  for (i=0;i<length;i++) {
    Serial.print((char)payload[i]);
    temp+=String((char)payload[i]);
  }
  Serial.println("]");
  comando=temp;
}
