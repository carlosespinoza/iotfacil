#include <DHT.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>  
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>

const char* nombreTarjeta="Dispositivo01";
const char* idTarjeta="Dispositivo01";
WiFiClient espClient;
WiFiClient espClient2mqtt;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

PubSubClient mqtt(espClient2mqtt);

#define DHTTYPE  DHT11
int tempHum=D4;
int pinLDR=A0;
int senHum=D0;
int trigger=D2;
int echo=D1;
long duracion=0;
int pinRele=D5;
DHT dht(tempHum, DHTTYPE);
float temperatura;
float humedad;
int pinRele2=D3;
int pinPIR=D6;
long tiempo=0;
int hayMovimiento;
String formattedDate;
String dayStamp;
String timeStamp;
unsigned long fechaHora;
String fechaHoraTexto;
String comando;

void setup()
{
   Serial.begin(9600);
   pinMode(pinLDR,INPUT); //Analogico
   pinMode(senHum,INPUT); //Digital
   pinMode(trigger,OUTPUT);
   pinMode(echo,INPUT);
   pinMode(pinRele,OUTPUT);
   pinMode(pinRele2,OUTPUT);
   pinMode(pinPIR,INPUT);
   tiempo=millis();
   hayMovimiento=0;

   //3600 por cada GMT
   //Mexico GMT-5=-18000
   timeClient.begin();
   timeClient.setTimeOffset(-18000);
  
   WiFiManager wifiManager;
   wifiManager.autoConnect("IoTFacil");
   mqtt.setServer("broker.hivemq.com",1883);
  
   mqtt.setCallback(callback);

   ConectarMQTT();
}

void loop()
{
  
  mqtt.loop();
  VerificaMovimiento();
  VerificaComando();
  if(comando!=""){
    EjecutaComando();
    comando="";
  }
  VerificaEnvio();
}

void VerificaEnvio(){
  if((millis()-tiempo)>10000){
    tiempo=millis();
    ObtenerFechaYHora();
    Serial.print(HumedadAmbiental());  //humedad
    Serial.print(" "); Serial.print(Temperatura()); //temperatura
    Serial.print(" "); Serial.print(Luminosidad()); //Analogico fotorestistencia
    Serial.print(" "); Serial.print(HumedadTierra()); //Digital Humedad
    Serial.print(" "); Serial.println(Distancia()); //distancia ultrasonico
  }
}

void VerificaComando(){
  if(Serial.available())
  {
    comando=Serial.readString();
    comando=comando.substring(0,comando.length()-1);
    Serial.println("Comando por puerto serie: [" + comando + "]");
  }
}


void EjecutaComando(){
  
        if(comando=="T?"){
          Serial.print("T=");
          Serial.println(Temperatura());
        }
      
        if(comando=="h?"){
          Serial.print("h=");
          Serial.println(HumedadTierra());
        }
      
        if(comando=="H?"){
          Serial.print("H=");
          Serial.println(HumedadAmbiental());
        }
        if(comando=="D?"){
          Serial.print("D=");
          Serial.println(Distancia());
        }
        if(comando=="L?"){
          Serial.print("L=");
          Serial.println(Luminosidad());
        }
        if(comando=="F1"){
          Foco(1);
        }
        if(comando=="F0"){
          Foco(0);
        }
        if(comando=="S?"){
          Serial.print(HumedadAmbiental());  //humedad
          Serial.print(" "); Serial.print(Temperatura()); //temperatura
          Serial.print(" "); Serial.print(Luminosidad()); //Analogico fotorestistencia
          Serial.print(" "); Serial.print(HumedadTierra()); //Digital Humedad
          Serial.print(" "); Serial.println(Distancia()); //distancia ultrasonico
        }
      
}

void VerificaMovimiento(){
  if(digitalRead(pinPIR)==HIGH){
    digitalWrite(pinRele2,HIGH);
      if(hayMovimiento==0){
        Serial.println("M");
        hayMovimiento=1;
      }
  }else{
    digitalWrite(pinRele2,LOW);
    hayMovimiento=0;
  }
}

int Temperatura(){
  float t;
  t=dht.readTemperature();
  if(!isnan(t))
  {
    temperatura=t;
  }
  return temperatura;
}

int HumedadAmbiental(){
  float h;
  h=dht.readHumidity();
  if(!isnan(h)){
      humedad=h;
  }
  return humedad;
}

int HumedadTierra(){
  if(digitalRead(senHum)==HIGH){
    return 0;
  }else{
    return 1;
  }
}

int Luminosidad(){
  return analogRead(pinLDR);
}

int Distancia(){
  digitalWrite(trigger,LOW);
  delayMicroseconds(2);
  digitalWrite(trigger,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger,LOW);
  duracion=pulseIn(echo,HIGH);
  return Distance(duracion);
}

long Distance(long time){
  long DistanceCalc;
  DistanceCalc=((time/2.9)/2); //distancia en mm
  return DistanceCalc/10; //regresa distancia en cm  
}

void Foco(int valor){
  if(valor==1){
      digitalWrite(pinRele,HIGH);
    }else{
      digitalWrite(pinRele,LOW);
    }
}

void ObtenerFechaYHora(){
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
  formattedDate = timeClient.getFormattedDate();
  int splitT = formattedDate.indexOf("T");
  dayStamp = formattedDate.substring(0, splitT);
  timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
  fechaHoraTexto=String(dayStamp[8])+String(dayStamp[9])+"/"+String(dayStamp[5])+String(dayStamp[6])+"/"+String(dayStamp[0])+String(dayStamp[1])+String(dayStamp[2])+String(dayStamp[3])+" "+String(timeStamp)+" ";
  Serial.print(fechaHoraTexto);
}
void ConectarMQTT(){
  if(!mqtt.connected()){
    mqtt.connect("IoTFacil");
    mqtt.subscribe("IoTFacil/Dispositivo01-Destino");
    EnviarComando("Tarjeta IoTFacil01 conectada");
  }
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Llego comando por MQTT [");
  Serial.print(topic);
  Serial.print("] [");
  String temp="";
  int i=0;
  for (i=0;i<length;i++) {
    Serial.print((char)payload[i]);
    temp+=String((char)payload[i]);
  }
  Serial.println("]");
  comando=temp;
}
void EnviarComando(String comando){
  ObtenerFechaYHora();
  Serial.println(comando);
  int len=comando.length()+1;
  char cmd[len];
  comando.toCharArray(cmd,len);
  mqtt.publish("IoTFacil/Dispositivo01-Origen",cmd);
}
